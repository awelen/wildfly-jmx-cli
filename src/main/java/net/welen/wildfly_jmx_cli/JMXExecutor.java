package net.welen.wildfly_jmx_cli;

import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.io.ByteArrayInputStream;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import org.jboss.dmr.ModelNode;

public class JMXExecutor {
    private final static Logger LOG = Logger.getLogger(JMXExecutor.class.getName());

	private MBeanServer server = ManagementFactory.getPlatformMBeanServer();

	public ModelNode listDomains() {
		ModelNode list = new ModelNode();
                for (String domain : server.getDomains()) {
                        list.add(domain);
                }
                return list;
	}

	public ModelNode findMBeans(String query) throws MalformedObjectNameException {
		ModelNode list = new ModelNode();
		for (ObjectName objectName : server.queryNames(new ObjectName(query), null)) {
			list.add(objectName.getCanonicalName());
		}
		return list;
	}

	public ModelNode getMBeanInfo(String objectName) throws IntrospectionException, InstanceNotFoundException,
			MalformedObjectNameException, ReflectionException {
		
		MBeanInfo info = server.getMBeanInfo(new ObjectName(objectName));
		ModelNode answer = new ModelNode();

		answer.add("ObjectName", objectName);
		answer.add("ClassName", info.getClassName());
		answer.add("Description", info.getDescription());
						
		ModelNode attributes = new ModelNode();
		for (MBeanAttributeInfo attribute : info.getAttributes()) {
			
			ModelNode attributeNode = new ModelNode();
			attributeNode.add("Readable", attribute.isReadable());
			attributeNode.add("Writeable", attribute.isWritable());
			attributeNode.add("Type", attribute.getType());
			attributeNode.add("Description", attribute.getDescription());
			
			attributes.add(attribute.getName(), attributeNode);
		}
		answer.add("Attributes", attributes);


		ModelNode operations = new ModelNode();
		for (MBeanOperationInfo operation : info.getOperations()) {
			ModelNode operationNode = new ModelNode();

			operationNode.add("ReturnType", operation.getReturnType());			
			operationNode.add("Description", operation.getDescription());			

			ModelNode parametersNode = new ModelNode();
			for (MBeanParameterInfo param : operation.getSignature()) {
				ModelNode parameterNode = new ModelNode();

				parameterNode.add("Type", param.getType());
                                parameterNode.add("Class", param.getClass().toString());                            
                                parameterNode.add("Description", param.getDescription());

				parametersNode.add(parameterNode);
			}
			operationNode.add("Parameters", parametersNode);

			operations.add(operation.getName(), operationNode);
		}
		answer.add("Operations", operations);


		return answer;
	}	
	
	public ModelNode getAttribute(String objectName, String attribute) throws InstanceNotFoundException,
			AttributeNotFoundException, MalformedObjectNameException, ReflectionException, MBeanException {
		ModelNode answer = new ModelNode();
		answer.add(nullStringFix(server.getAttribute(new ObjectName(objectName), attribute)));
		return answer;
	}

	public void setAttribute(String objectName, String name, String value) throws Exception {
		server.setAttribute(new ObjectName(objectName), new Attribute(name, createInstance(value)));
	}

	public ModelNode invoke(String objectName, String operationName, String params, String signatureString) throws Exception {
		ModelNode answer = new ModelNode();
		if (params == null || params.trim().isEmpty()) {
			// Invoke method
			answer.add(nullStringFix(server.invoke(new ObjectName(objectName), operationName, null, new String[0])));
		} else {
			// Create parameter objects
			Object[] paramObjects = (Object[]) createInstance("<array class=\"java.lang.Object\">" + params + "</array>");
			// Create signature
			String[] signature;
			if (signatureString == null) {
				signature = new String[paramObjects.length];
				for (int i=0; i<paramObjects.length; i++) {
					String signatureClassName = paramObjects[i].getClass().getName(); 
					signature[i] = signatureClassName;
					LOG.log(Level.FINE, "Parameter #" + i + ": " + signatureClassName);
				}
			} else {				
				signature = signatureString.split(",");
			}
			// Invoke method
			answer.add(nullStringFix(server.invoke(new ObjectName(objectName), operationName, paramObjects, signature)));
		}
		return answer;
	}

	private String nullStringFix(Object value) {
		if (value == null) {
			return "null";
		}
		return value.toString();
	}

	private static class SimpleExceptionListener implements ExceptionListener {
		private Exception e = null;

		public void exceptionThrown(Exception e) {
			this.e = e;
		}

		Exception getException() {
			return e;
		}
	}

	private Object createInstance(String data) throws Exception {
		if (data == null || data.trim().isEmpty()) {
			return null;
		}

		String xml = "<java class=\"java.beans.XMLDecoder\">" + data + "</java>";
		LOG.log(Level.FINE, "XML to parse: " + xml);

		XMLDecoder decoder = null;
		try {
			decoder = new XMLDecoder(new ByteArrayInputStream(xml.getBytes()));
			SimpleExceptionListener aListener = new SimpleExceptionListener();
			decoder.setExceptionListener(aListener);

			Object answer = decoder.readObject();

			Exception exception = aListener.getException();
			if (exception != null) {
				LOG.log(Level.SEVERE, exception.getMessage(), exception);
				throw exception;
			}

			return answer;
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new Exception("Input invalid: " + data, e);
		} finally {
			if (decoder != null) {
				decoder.close();
			}
		}
	}
	

}
