package net.welen.wildfly_jmx_cli.extension;

import org.jboss.as.controller.SimpleAttributeDefinitionBuilder;
import org.jboss.as.controller.SimpleOperationDefinitionBuilder;
import org.jboss.as.controller.SimpleResourceDefinition;
import org.jboss.as.controller.registry.ManagementResourceRegistration;
import org.jboss.dmr.ModelType;

import net.welen.wildfly_jmx_cli.FindMBeans;
import net.welen.wildfly_jmx_cli.GetAttribute;
import net.welen.wildfly_jmx_cli.GetMBeanInfo;
import net.welen.wildfly_jmx_cli.Invoke;
import net.welen.wildfly_jmx_cli.ListDomains;
import net.welen.wildfly_jmx_cli.SetAttribute;

/**
 * @author <a href="mailto:tcerar@redhat.com">Tomaz Cerar</a>
 */
public class SubsystemDefinition extends SimpleResourceDefinition {
	public static final SubsystemDefinition INSTANCE = new SubsystemDefinition();

	private SubsystemDefinition() {
		super(SubsystemExtension.SUBSYSTEM_PATH, SubsystemExtension.getResourceDescriptionResolver(null),
				// We always need to add an 'add' operation
				SubsystemAdd.INSTANCE,
				// Every resource that is added, normally needs a remove operation
				SubsystemRemove.INSTANCE);
	}

	@Override
	public void registerOperations(ManagementResourceRegistration resourceRegistration) {
		super.registerOperations(resourceRegistration);
		// you can register additional operations here

		resourceRegistration.registerOperationHandler(
				new SimpleOperationDefinitionBuilder(SubsystemExtension.SUBSYSTEM_LISTDOMAINS_OP,
						SubsystemExtension.getResourceDescriptionResolver(null)).build(),
				ListDomains.INSTANCE);

		resourceRegistration.registerOperationHandler(new SimpleOperationDefinitionBuilder(
				SubsystemExtension.SUBSYSTEM_FINDMBEANS_OP, SubsystemExtension.getResourceDescriptionResolver(null))
						.setParameters(SimpleAttributeDefinitionBuilder.create("objectName", ModelType.STRING, false).build())
						.build(),
				FindMBeans.INSTANCE);

		resourceRegistration.registerOperationHandler(new SimpleOperationDefinitionBuilder(
				SubsystemExtension.SUBSYSTEM_GETMBEANINFO_OP, SubsystemExtension.getResourceDescriptionResolver(null))
						.setParameters(SimpleAttributeDefinitionBuilder.create("objectName", ModelType.STRING, false).build())
						.build(),
				GetMBeanInfo.INSTANCE);

		resourceRegistration.registerOperationHandler(new SimpleOperationDefinitionBuilder(
				SubsystemExtension.SUBSYSTEM_GETATTRIBUTE_OP, SubsystemExtension.getResourceDescriptionResolver(null))
						.setParameters(
								SimpleAttributeDefinitionBuilder.create("objectName", ModelType.STRING, false).build(),
								SimpleAttributeDefinitionBuilder.create("attributeName", ModelType.STRING, false).build())
						.build(),
				GetAttribute.INSTANCE);

		resourceRegistration.registerOperationHandler(new SimpleOperationDefinitionBuilder(
				SubsystemExtension.SUBSYSTEM_SETATTRIBUTE_OP, SubsystemExtension.getResourceDescriptionResolver(null))
						.setParameters(
								SimpleAttributeDefinitionBuilder.create("objectName", ModelType.STRING, false).build(),
								SimpleAttributeDefinitionBuilder.create("attributeName", ModelType.STRING, false).build(),
								SimpleAttributeDefinitionBuilder.create("value", ModelType.STRING, false).build())
						.build(),
				SetAttribute.INSTANCE);

		resourceRegistration.registerOperationHandler(new SimpleOperationDefinitionBuilder(
				SubsystemExtension.SUBSYSTEM_INVOKE_OP, SubsystemExtension.getResourceDescriptionResolver(null))
						.setParameters(
								SimpleAttributeDefinitionBuilder.create("objectName", ModelType.STRING, false).build(),
								SimpleAttributeDefinitionBuilder.create("operationName", ModelType.STRING, false).build(),
								SimpleAttributeDefinitionBuilder.create("parameters", ModelType.STRING, false).build(),
								SimpleAttributeDefinitionBuilder.create("signature", ModelType.STRING, false).build())
						.build(),
				Invoke.INSTANCE);

	}

	@Override
	public void registerAttributes(ManagementResourceRegistration resourceRegistration) {
		// you can register attributes here
	}
}
