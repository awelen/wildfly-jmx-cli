package net.welen.wildfly_jmx_cli;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jboss.as.controller.OperationContext;
import org.jboss.as.controller.OperationFailedException;
import org.jboss.as.controller.OperationStepHandler;
import org.jboss.dmr.ModelNode;

public class Invoke implements OperationStepHandler {
    private final static Logger LOG = Logger.getLogger(Invoke.class.getName());

	static public final Invoke INSTANCE = new Invoke();

	private JMXExecutor executor = new JMXExecutor();

	private Invoke() {
	}

	@Override
	public void execute(OperationContext opContext, ModelNode model) throws OperationFailedException {
		if (!model.get("objectName").isDefined()) {
			throw new OperationFailedException("Parameter \"objectName\" is mandatory");
		}
		if (!model.get("operationName").isDefined()) {
			throw new OperationFailedException("Parameter \"operationName\" is mandatory");
		}
		
		String objectName = model.get("objectName").asString();		
		String operationName = model.get("operationName").asString();
		String parameters = null;
		if (model.get("parameters").isDefined()) {
			parameters = model.get("parameters").asString();
		}
		String signature = null;
		if (model.get("signature").isDefined()) {
			signature = model.get("signature").asString();
		}
		
		try {
			opContext.getResult().set(executor.invoke(objectName, operationName, parameters, signature));
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new OperationFailedException(e.getClass() + ": " + e.getMessage());
		}	
	}
}

