package net.welen.wildfly_jmx_cli;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MalformedObjectNameException;

import org.jboss.as.controller.OperationContext;
import org.jboss.as.controller.OperationFailedException;
import org.jboss.as.controller.OperationStepHandler;
import org.jboss.dmr.ModelNode;

public class FindMBeans implements OperationStepHandler {
    private final static Logger LOG = Logger.getLogger(FindMBeans.class.getName());

	static public final FindMBeans INSTANCE = new FindMBeans();

	private JMXExecutor executor = new JMXExecutor();

	private FindMBeans() {
	}

	@Override
	public void execute(OperationContext opContext, ModelNode model) throws OperationFailedException {
		if (!model.get("objectName").isDefined()) {
			throw new OperationFailedException("Parameter \"objectName\" is mandatory");
		}
		
		String objectName = model.get("objectName").asString();		
		try {
			opContext.getResult().set(executor.findMBeans(objectName));
		} catch (MalformedObjectNameException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new OperationFailedException(e.getClass() + ": " + e.getMessage());
		}	
	}
}

