package net.welen.wildfly_jmx_cli;

import org.jboss.as.controller.OperationContext;
import org.jboss.as.controller.OperationFailedException;
import org.jboss.as.controller.OperationStepHandler;
import org.jboss.dmr.ModelNode;

public class ListDomains implements OperationStepHandler {
	
	static public final ListDomains INSTANCE = new ListDomains();

	private JMXExecutor executor = new JMXExecutor();

	private ListDomains() {
	}

	@Override
	public void execute(OperationContext opContext, ModelNode model) throws OperationFailedException {
		opContext.getResult().set(executor.listDomains());
	}
}

