package net.welen.wildfly_jmx_cli;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jboss.as.controller.OperationContext;
import org.jboss.as.controller.OperationFailedException;
import org.jboss.as.controller.OperationStepHandler;
import org.jboss.dmr.ModelNode;

public class GetMBeanInfo implements OperationStepHandler {
    private final static Logger LOG = Logger.getLogger(GetMBeanInfo.class.getName());

	static public final GetMBeanInfo INSTANCE = new GetMBeanInfo();

	private JMXExecutor executor = new JMXExecutor();

	private GetMBeanInfo() {
	}

	@Override
	public void execute(OperationContext opContext, ModelNode model) throws OperationFailedException {
		if (!model.get("objectName").isDefined()) {
			throw new OperationFailedException("Parameter \"objectName\" is mandatory");
		}

		String objectName = model.get("objectName").asString();		
		try {
			opContext.getResult().set(executor.getMBeanInfo(objectName));
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new OperationFailedException(e.getClass() + ": " + e.getMessage());
		}	
	}
}

