package net.welen.wildfly_jmx_cli;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jboss.as.controller.OperationContext;
import org.jboss.as.controller.OperationFailedException;
import org.jboss.as.controller.OperationStepHandler;
import org.jboss.dmr.ModelNode;

public class GetAttribute implements OperationStepHandler {
    private final static Logger LOG = Logger.getLogger(GetAttribute.class.getName());

	static public final GetAttribute INSTANCE = new GetAttribute();

	private JMXExecutor executor = new JMXExecutor();

	private GetAttribute() {
	}

	@Override
	public void execute(OperationContext opContext, ModelNode model) throws OperationFailedException {
		if (!model.get("objectName").isDefined()) {
			throw new OperationFailedException("Parameter \"objectName\" is mandatory");
		}
		if (!model.get("attributeName").isDefined()) {
			throw new OperationFailedException("Parameter \"attributeName\" is mandatory");
		}

		String objectName = model.get("objectName").asString();		
		String attributeName = model.get("attributeName").asString();
		try {
			opContext.getResult().set(executor.getAttribute(objectName, attributeName));
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new OperationFailedException(e.getClass() + ": " + e.getMessage());
		}	
	}
}

