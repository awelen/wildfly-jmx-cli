<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>net.welen.WildFly-JMX-CLI</groupId>
	<artifactId>WildFly-JMX-CLI</artifactId>
	<version>1.3.0</version>
	<packaging>jar</packaging>

	<name>WildFly-JMX-CLI</name>
	<inceptionYear>2019</inceptionYear>
	<description>Accessing JMX through WildFly CLI</description>
	<url>https://bitbucket.org/awelen/wildfly-jmx-cli</url>

	<scm>
		<connection>scm:hg:https://bitbucket.org/awelen/wildfly-jmx-cli.git</connection>
		<developerConnection>scm:hg:https://bitbucket.org/awelen/wildfly-jmx-cli.git</developerConnection>		
		<url>https://bitbucket.org/awelen/wildfly-jmx-cli</url>
	</scm>

	<issueManagement>
		<system>Bitbucket</system>
		<url>https://bitbucket.org/awelen/wildfly-jmx-cli/issues</url>
	</issueManagement>

	<distributionManagement>
		<snapshotRepository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
		<repository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
		</repository>
	</distributionManagement>

	<licenses>
		<license>
			<name>The GNU Lesser General Public License, Version 3.0</name>
			<url>http://www.gnu.org/licenses/lgpl-3.0.txt</url>
			<comments>See readme.md for more information</comments>
		</license>
	</licenses>

	<developers>
		<developer>
			<name>Anders Welén</name>
			<email>anders@welen.net</email>
		</developer>
	</developers>

	<properties>
		<version.wildfly.core>21.1.0.Final</version.wildfly.core>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>2.2.1</version>
					<executions>
						<execution>
							<id>attach-sources</id>
							<goals>
								<goal>jar-no-fork</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>2.9.1</version>
				</plugin>				
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-gpg-plugin</artifactId>
					<version>1.5</version>
				</plugin>
				<plugin>
					<groupId>org.sonatype.plugins</groupId>
					<artifactId>nexus-staging-maven-plugin</artifactId>
					<version>1.6.7</version>
				</plugin>			
			</plugins>
		</pluginManagement>
			
		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptor>src/assembly/module.xml</descriptor>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	
	<!-- Profile containing requirements by Central Nexus deployment -->
	<profiles>
		<profile>
			<id>release</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-source-plugin</artifactId>
						<executions>
							<execution>
								<id>attach-sources</id>
								<goals>
									<goal>jar-no-fork</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
						<executions>
							<execution>
								<id>attach-javadocs</id>
								<goals>
									<goal>jar</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.sonatype.plugins</groupId>
						<artifactId>nexus-staging-maven-plugin</artifactId>
						<extensions>true</extensions>
						<configuration>
							<serverId>ossrh</serverId>
							<nexusUrl>https://oss.sonatype.org/</nexusUrl>
							<autoReleaseAfterClose>true</autoReleaseAfterClose>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<dependencies>
		<dependency>
			<groupId>org.wildfly.core</groupId>
			<artifactId>wildfly-controller</artifactId>
			<version>${version.wildfly.core}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.wildfly.core</groupId>
			<artifactId>wildfly-server</artifactId>
			<version>${version.wildfly.core}</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>
</project>
