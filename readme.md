# WildFly-JMX-CLI - Accessing JMX through WildFly CLI

Copyright 2019 - 2021 Anders Wel�n, anders@welen.net
(See also "copyright.txt")

# Description
The MBean standard is used by various Java frameworks and can be access using standard tools like JConsole etc. Even if there are tools like Jolokia available to provide HTTP based access there is no command-line tool like the old and useful *twiddle* shipped in later JBoss/WildFly versions. My friend Arnold Johansson has [ported](https://developer.jboss.org/people/arnoldjohansson/blog/2012/04/14/twiddle-standalone) *twiddle* to later JBoss/WildFly versions, but I always thought that the new execellent CLI of JBoss/WildFly lacks a *bridge* to access JMX, so I built *WildFly-JMX-CLI*.

*WildFly-JMX-CLI* supports WildFly 8 and up, but should also work on JBoss EAP versions derived from them. If the latest version doesn't work for an older WildFly version try version 1.1.*.
 
# How to install
1. Unzip *WildFly-JMX-CLI-X.X.X-module.zip* in the WildFly module directory
2. Run the following CLI commands to install the module:

```
        /extension=net.welen.jmx-cli:add(module="net.welen.jmx-cli")
        /subsystem=jmx-cli:add()
```

# How to use
Use the exampled below as a starting point to understand how to use this simple subsystem. To understand more about *JMX*, *MBeans* and *ObjectName* syntax you can start reading [here](https://docs.oracle.com/javase/8/docs/technotes/guides/jmx/overview/intro.html).

For handling transformation between CLI input to Java classes/primitives the subsystem uses [*java.beans.XMLEncoder*](https://docs.oracle.com/javase/7/docs/api/java/beans/XMLEncoder.html).  

## List all MBean Domains
``` 
/subsystem=jmx-cli:listDomains()
```

## Search for MBeans
``` 
/subsystem=jmx-cli:findMBeans(objectName="java.lang:*")
```

## Getting information about a MBean 
```
/subsystem=jmx-cli:getMBeanInfo(objectName="java.lang:type=Memory")
```

## Getting MBean attributes 
``` 
/subsystem=jmx-cli:getAttribute(objectName="java.lang:type=ClassLoading", attributeName="Verbose")
```

## Setting MBean attributes 
``` 
/subsystem=jmx-cli:setAttribute(objectName="java.lang:type=ClassLoading", attributeName="Verbose", value="<boolean>false</boolean>")
```

## Invoking a MBean operation 
```
/subsystem=jmx-cli:invoke(objectName="java.lang:type=Memory", operationName="gc")
/subsystem=jmx-cli:invoke(objectName="java.lang:type=Threading", operationName="getThreadCpuTime", parameters="<long>1</long>", signature="long")
/subsystem=jmx-cli:invoke(objectName="java.lang:type=Threading", operationName="dumpAllThreads", parameters="<boolean>true</boolean><boolean>true</boolean>", signature="boolean,boolean")
```
*parameters* and *signature* can be omitted if not needed and if *signature* is omitted but needed, it will be generated automatically by guessing based on the parameter objects.
